use std::collections::HashMap;

use route_recognizer::Router as Recognizer;

use crate::*;

pub trait Router {
    fn add_route(&mut self, method: hyper::Method, path: &str, route: Route);
    fn dispatch(&self, req: SkeletonRequest) -> MiddlewareResponse;
}

pub fn default_router() -> RouterImpl {
    RouterImpl {
        routes: std::collections::HashMap::new(),
    }
}

pub struct RouterImpl {
    routes: HashMap<hyper::Method, Recognizer<Route>>,
}

impl RouterImpl {
    fn not_found(&self, req: SkeletonRequest, msg: String) -> MiddlewareResponse {
        use futures_util::future::FutureExt;

        let mut resp = hyper::Response::new(hyper::body::Body::from(msg));
        *resp.status_mut() = hyper::StatusCode::NOT_FOUND;

        let ok_resp = std::result::Result::Ok(resp);
        async { InternalResponse::new(req, ok_resp) }.boxed()
    }

    pub fn add(&mut self, route: RouteDefinition) {
        self.add_route(route.method, route.path, route.handler)
    }
}

impl Router for RouterImpl {
    fn add_route(&mut self, method: hyper::Method, path: &str, route: Route) {
        if !self.routes.contains_key(&method) {
            self.routes.insert(method.to_owned(), Recognizer::new());
        }

        let path_routes = self.routes.get_mut(&method).unwrap();
        path_routes.add(&path.to_owned(), route);
    }

    fn dispatch(&self, mut req: SkeletonRequest) -> MiddlewareResponse {
        let uri = req.uri();
        let path = uri.path().trim_end();
        let method = req.method();

        if self.routes.get(&method).is_none() {
            let msg = format!("Method {} to route {} not found", &method, &path.to_owned());
            return self.not_found(req, msg);
        }

        let routes = self.routes.get(&method).unwrap();
        let matches = routes.recognize(&path.to_owned());
        if matches.is_err() {
            let msg = format!(
                "{} not found in routes: {}",
                &path.to_owned(),
                matches.err().unwrap()
            );
            return self.not_found(req, msg);
        }
        let matches = matches.unwrap();

        req.set_route_params(matches.params);

        match matches.handler {
            Route::Core(func) => {
                func(req)
            }
        }
    }
}

pub struct RouteDefinition {
    pub method: hyper::Method,
    pub path: &'static str,
    pub handler: Route,
}

#[macro_export]
macro_rules! route {
    ($http_method:ident, $path:tt, $struct:ident.$method:ident) => {{
        use std::sync::Arc;
        use futures_util::future::FutureExt;
        use futures_util::stream::StreamExt;

        let borrowed = Arc::clone(&$struct);

        RouteDefinition {
            method: Method::$http_method,
            path: $path,
            handler: Route::Core(Box::new(move |req| {
                let borrowed = borrowed.clone();

                async move {
                    let mut req = req;
                    let mut handler = borrowed;
                    let response_future = handler.$method(&mut req);
                    let resp = response_future.await;
                    InternalResponse::new(req, resp.into_response())
                }
                    .boxed()
            })),
        }
    }};

    ($http_method:ident, $path:tt, $struct:ident.$method:ident, $transformer:expr) => {{
        use std::sync::Arc;
        use futures_util::future::FutureExt;
        use futures_util::stream::StreamExt;

        let borrowed = Arc::clone(&$struct);

        RouteDefinition {
            method: Method::$http_method,
            path: $path,
            handler: Route::Core(Box::new(move |req| {
                let borrowed = borrowed.clone();

                async move {
                    let mut req = req;
                    let mut handler = borrowed;
                    let response_future = handler.$method(&mut req);
                    let resp = response_future.await;
                    let resp = resp.into_response();

                    InternalResponse::new(req, $transformer(resp))
                }
                    .boxed()
            })),
        }
    }};
}

pub fn make_controller<T>(obj: T) -> std::sync::Arc<T> {
    std::sync::Arc::new(obj)
}

pub fn make_shared_data<T>(data: T) -> Shared<T> {
    std::sync::Arc::new(futures::lock::Mutex::new(data))
}

/*
router.add({
    let borrowed = Arc::clone(&ping_handler);

    RouteDefinition {
        method: Method::POST,
        path: "/form",
        handler: Route::Core(Box::new(move |req| {
            let borrowed = borrowed.clone();

            async move {
                let mut req = req;
                let mut handler = borrowed.lock().await;
                let response_future = handler.formdata(&mut req);
                let resp = response_future.await;
                InternalResponse::new(req, resp.into_response())
            }.boxed()
        })),
    }
});
*/
