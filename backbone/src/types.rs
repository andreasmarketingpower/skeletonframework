use async_trait::async_trait;

pub type RawRequest = hyper::http::Request<hyper::Body>;
pub type RawResponse = hyper::http::Response<hyper::Body>;

pub type ConcurrentError = dyn std::error::Error + Send + Sync;
pub type ResponseResult = std::result::Result<RawResponse, Box<ConcurrentError>>;

type PinnedResponseFuture<'a> =
    std::pin::Pin<Box<dyn futures::Future<Output = InternalResponse> + Send + 'a>>;

// pub type SkeletonRequest = RawRequest;
pub type MiddlewareResponse = PinnedResponseFuture<'static>;
// pub type MiddlewareResponse2 = Box<dyn futures::Future<Output = ResponseResult> + Send>;

pub type Middleware = Box<dyn Fn(SkeletonRequest) -> MiddlewareResponse + Send + Sync>;
pub type ErrorMiddleware = Box<dyn Fn(&ConcurrentError) -> MiddlewareResponse + Send + Sync>;

pub enum Route {
    Core(Middleware),
}

pub type Shared<T> = std::sync::Arc<futures::lock::Mutex<T>>;

pub struct InternalResponse {
    request: Option<SkeletonRequest>,
    response: Option<ResponseResult>,
}

impl InternalResponse {
    pub fn new(request: SkeletonRequest, response: ResponseResult) -> Self {
        Self {
            request: Some(request),
            response: Some(response),
        }
    }

    pub fn consume_request(&mut self) -> SkeletonRequest {
        self.request.take().unwrap()
    }

    pub fn consume_response(&mut self) -> ResponseResult {
        self.response.take().unwrap()
    }
}

pub struct SkeletonRequest {
    body: Option<hyper::Body>,

    // parts: hyper::http::request::Parts,
    method: hyper::http::method::Method,
    uri: hyper::http::uri::Uri,
    version: hyper::http::Version,
    headers: hyper::http::header::HeaderMap<hyper::http::header::HeaderValue>,
    extensions: hyper::http::Extensions,

    params: Option<route_recognizer::Params>,
}

impl SkeletonRequest {
    pub fn from_hyper_request(req: RawRequest) -> Self {
        let (parts, body) = req.into_parts();

        SkeletonRequest {
            body: Some(body),
            method: parts.method.clone(),
            uri: parts.uri.clone(),
            version: parts.version,
            headers: parts.headers,
            extensions: hyper::http::Extensions::new(),
            params: None,
        }
    }

    pub(crate) fn set_route_params(&mut self, params: route_recognizer::Params) {
        self.params.replace(params);
    }

    pub fn param(&self, name: &str) -> Option<&str> {
        self.params.as_ref().and_then(|p| p.find(&name))
    }

    pub fn uri(&self) -> hyper::http::uri::Uri {
        self.uri.clone()
    }

    pub fn version(&self) -> hyper::http::Version {
        self.version
    }

    pub fn method(&self) -> hyper::http::Method {
        self.method.clone()
    }

    pub fn headers(&self) -> &hyper::http::header::HeaderMap<hyper::http::header::HeaderValue> {
        &self.headers
    }

    pub fn headers_mut(
        &mut self,
    ) -> &mut hyper::http::header::HeaderMap<hyper::http::header::HeaderValue> {
        &mut self.headers
    }

    pub fn extensions(&self) -> &hyper::http::Extensions {
        &self.extensions
    }

    pub fn extensions_mut(&mut self) -> &mut hyper::http::Extensions {
        &mut self.extensions
    }

    pub async fn copy_body(&mut self) -> hyper::body::Body {
        let body = self.body.take().unwrap();

        let new_body = hyper::body::to_bytes(body).await.unwrap_or_default();

        self.body = Some(hyper::body::Body::from(new_body.clone()));

        hyper::body::Body::from(new_body)
    }

    #[allow(clippy::wrong_self_convention)]
    pub fn into_body(&mut self) -> hyper::body::Body {
        self.body.take().unwrap()
    }
}

#[async_trait]
pub trait BodyExt {
    async fn as_json<T>(&mut self) -> Option<T>
    where
        T: serde::de::DeserializeOwned;
    async fn as_bytes(&mut self) -> Option<Vec<u8>>;
    async fn as_string(&mut self) -> Option<String>;
}

#[async_trait]
impl BodyExt for hyper::body::Body {
    async fn as_json<T>(&mut self) -> Option<T>
    where
        T: serde::de::DeserializeOwned,
    {
        if let Some(body) = self.as_string().await {
            serde_json::from_str::<T>(&body).ok()
        } else {
            None
        }
    }

    async fn as_bytes(&mut self) -> Option<Vec<u8>> {
        if let Ok(new_body) = hyper::body::to_bytes(self).await {
            let body: Vec<u8> = Vec::from(new_body.as_ref());
            return Some(body);
        }

        None
    }

    async fn as_string(&mut self) -> Option<String> {
        if let Ok(new_body) = hyper::body::to_bytes(self).await {
            let body: Vec<u8> = Vec::from(new_body.as_ref());
            return Some(String::from_utf8_lossy(&body).to_string());
        }

        None
    }
}

pub trait SkeletonResponseFromBox {
    fn into_response_from_box(self: Box<Self>) -> ResponseResult;
}

impl<T: SkeletonResponse> SkeletonResponseFromBox for T {
    fn into_response_from_box(self: Box<Self>) -> ResponseResult {
        SkeletonResponse::into_response(*self)
    }
}

pub trait SkeletonResponse: SkeletonResponseFromBox {
    fn into_response(self) -> ResponseResult;
}


impl SkeletonResponse for hyper::http::response::Response<hyper::body::Body> {
    fn into_response(self) -> ResponseResult {
        Ok(self)
    }
}

impl<T: SkeletonResponse> SkeletonResponse for std::result::Result<T, Box<ConcurrentError>> {
    fn into_response(self) -> ResponseResult {
        if let Ok(resp) = self {
            return resp.into_response();
        }

        let error = self.err().unwrap();
        error.into_response()
    }
}

impl SkeletonResponse for Box<ConcurrentError> {
    fn into_response(self) -> ResponseResult {
        Err(self)
    }
}

impl SkeletonResponse for Box<dyn SkeletonResponse> {
    fn into_response(self) -> ResponseResult {
        self.into_response_from_box()
    }
}