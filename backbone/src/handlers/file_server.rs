use crate::*;

pub struct FileServer {
    base_query_dir: std::path::PathBuf,
    base_filesystem_dir: std::path::PathBuf,
}

impl FileServer {
    pub fn new(base_query_dir: std::path::PathBuf, base_filesystem_dir: std::path::PathBuf) -> Self {
        Self {
            base_query_dir,
            base_filesystem_dir
        }
    }

    pub async fn assets(&self, req: &mut SkeletonRequest) -> Box<dyn SkeletonResponse> {
        use futures_util::stream::*;
        use tokio_util::codec::{FramedRead, BytesCodec};

        let uri = req.uri();
        let path = std::path::PathBuf::from(uri.path());

        let stripped_path = path.strip_prefix(&self.base_query_dir);
        if let Err(err_msg) = stripped_path {
            return self.not_found(format!("File '{}' not found: {}", uri.path(), err_msg));
        }

        let base_path = self.base_filesystem_dir.clone().join(stripped_path.unwrap());
        if let Some(path_str) = base_path.clone().to_str() {
            if !self.is_safe_path(path_str) {
                return self.not_found(format!("File '{}' not found in scope", uri.path()));
            }
        }

        let content_type = mime_guess::from_path(base_path.clone()).first_or_octet_stream();

        let maybe_file = tokio::fs::File::open(base_path.clone()).await;

        if let Ok(file) = maybe_file {
            // let stream = FramedRead::new(file, BytesCodec::new()).map(|r| {
            //     r.map(|b| b.freeze())
            // });

            let stream = FramedRead::new(file, BytesCodec::new()).map_ok(bytes::BytesMut::freeze);
            let response = Response::builder()
                .header(hyper::http::header::CONTENT_TYPE, content_type.to_string())
                .body(Body::wrap_stream(stream))
                .unwrap();

            Box::new(response)
        } else {
            let err_msg = maybe_file.err().unwrap();
            self.not_found(format!("File '{}' cannot be opened: {}", base_path.display(), err_msg))
        }
    }

    fn not_found(&self, msg: String) -> Box<dyn SkeletonResponse> {
        Box::new(Text(msg, StatusCode::NOT_FOUND))
    }

    fn is_safe_path(&self, s: &str) -> bool {
        let parts = s.split('/');
        
        let mut num_segments = 0;
        
        for s in parts {
            if s == ".." {
                num_segments -= 1;
                
                if num_segments < 0 {
                    return false;
                }
                continue;
            }
        
            num_segments += 1;
        }
        
        true
    }
}