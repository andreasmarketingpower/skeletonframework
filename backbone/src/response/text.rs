use crate::*;

pub struct Text(pub String, pub hyper::StatusCode);

impl SkeletonResponse for Text {
    fn into_response(self) -> ResponseResult {
        let resp = Response::builder()
            .status(self.1)
            .body(Body::from(self.0))
            .unwrap();

        Ok(resp)
    }
}