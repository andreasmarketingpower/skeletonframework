use crate::*;

pub struct Json<T>(pub T, pub hyper::StatusCode)
where
    T: serde::ser::Serialize;

impl<T> SkeletonResponse for Json<T>
where
    T: serde::ser::Serialize,
{
    fn into_response(self) -> ResponseResult {
        let mut resp = Response::builder()
            .status(self.1)
            .body(Body::from(serde_json::to_string(&self.0).unwrap()))
            .unwrap();

        resp.headers_mut().insert(
            "Content-Type",
            hyper::header::HeaderValue::from_static("application/json"),
        );
        Ok(resp)
    }
}
