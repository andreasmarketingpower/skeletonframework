use futures::prelude::*;

use crate::Router;
use crate::{AroundMiddleware, ErrorMiddleware, NoMiddleware};
use crate::{RawRequest, ResponseResult, SkeletonRequest};

use std::sync::Arc;

pub struct Web<R: Router, M: AroundMiddleware> {
    router: R,

    around_middlewares: M,

    global_error_handler: Option<ErrorMiddleware>,
}

impl<R> Web<R, NoMiddleware>
where
    R: Router + Send + 'static,
{
    pub fn new(router: R) -> Self {
        Web {
            router,

            around_middlewares: NoMiddleware,
            global_error_handler: None,
        }
    }
}

impl<R, M> Web<R, M>
where
    R: Router + Send + Sync + 'static,
    M: AroundMiddleware + Send + Sync + 'static,
{
    pub fn with_middlewares<MNext>(self, middleware: MNext) -> Web<R, impl AroundMiddleware>
    where
        MNext: AroundMiddleware,
    {
        Web {
            router: self.router,
            around_middlewares: self.around_middlewares.and_then(middleware),
            global_error_handler: self.global_error_handler,
        }
    }

    pub async fn listen(self, bind_addr: std::net::SocketAddr) {
        use hyper::server::conn::AddrStream;
        use hyper::service::*;
        use hyper::{Body, Error, Request};

        let router = Arc::new(self.router);

        let context = Arc::new(RequestContext {
            around_middlewares: self.around_middlewares,
            global_error_handler: self.global_error_handler,
        });

        let make_svc = make_service_fn(move |_socket: &AddrStream| {
            // let remote_addr = socket.remote_addr();

            let request_handler = {
                let shared_router = Arc::clone(&router);
                let shared_context = Arc::clone(&context);

                move |req: Request<Body>| {
                    let request_router = Arc::clone(&shared_router);
                    let request_context = Arc::clone(&shared_context);

                    async move { handle_request(&*request_router, &request_context, req).await }
                }
            };

            future::ok::<_, Error>(service_fn(request_handler))
        });

        let server = hyper::server::Server::bind(&bind_addr).serve(make_svc);

        println!("Listening on http://{}", &bind_addr);

        if let Err(e) = server.await {
            eprintln!("server error: {}", e);
        }
    }
}

struct RequestContext<M: AroundMiddleware> {
    pub around_middlewares: M,
    pub global_error_handler: Option<ErrorMiddleware>,
}

async fn handle_request(
    router: &impl Router,
    context: &RequestContext<impl AroundMiddleware>,
    request: RawRequest,
) -> ResponseResult {
    let mut req = SkeletonRequest::from_hyper_request(request);
    context.around_middlewares.wrap_request(&mut req);

    let mut wrapped_response = router.dispatch(req).await;

    let req = wrapped_response.consume_request();
    let mut resp: ResponseResult = wrapped_response.consume_response();

    match &mut resp {
        Ok(r) => context.around_middlewares.wrap_response(&req, r),
        Err(err) => {
            if let Some(handler_func) = &context.global_error_handler {
                println!("before exec error handler");

                let mut internal_resp_error = handler_func(err.as_ref()).await;
                let resp_error: ResponseResult = internal_resp_error.consume_response();
                return resp_error;
            }
        }
    }

    resp
}
