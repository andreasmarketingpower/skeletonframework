use crate::*;

pub use ::tera::*;

pub struct ViewEngineTeraMiddleware {
    template_dir: String,
}

impl ViewEngineTeraMiddleware {
    pub fn new(template_dir: &str) -> ViewEngineTeraMiddleware {
        ViewEngineTeraMiddleware {
            template_dir: template_dir.to_owned(),
        }
    }

    pub fn parse(&self) -> ::tera::Result<Tera> {
        Tera::new(&self.template_dir)
    }
}

impl AroundMiddleware for ViewEngineTeraMiddleware {
    fn wrap_request(&self, req: &mut SkeletonRequest) {
        let parsed = self.parse();

        // println!("Registering view engine");
        req.extensions_mut().insert(parsed.unwrap());
    }

    fn wrap_response(&self, _orig: &SkeletonRequest, _resp: &mut RawResponse) {}
}

pub trait View {
    fn view(&self, template_name: &str) -> TeraBuilder<'_>;
}

impl View for SkeletonRequest {
    fn view(&self, template_name: &str) -> TeraBuilder<'_> {
        let instance = self.extensions().get::<Tera>().unwrap();

        TeraBuilder {
            instance,
            context: Context::new(),
            template_name: template_name.to_owned(),
        }
    }
}

pub struct TeraBuilder<'a> {
    instance: &'a Tera,
    context: tera::Context,

    template_name: String,
}

impl<'a> TeraBuilder<'a> {
    pub fn set<T: serde::ser::Serialize + ?Sized>(
        &mut self,
        key: &str,
        val: &T,
    ) -> &mut TeraBuilder<'a> {
        self.context.insert(key, val);
        self
    }

    pub fn build(&self) -> ::tera::Result<String> {
        self.instance
            .render(self.template_name.as_str(), &self.context)
    }
}

impl SkeletonResponse for ::tera::Result<String> {
    fn into_response(self) -> ResponseResult {
        match self {
            Ok(content) => {
                let resp = Response::builder()
                    .status(StatusCode::OK)
                    .body(Body::from(content))
                    .unwrap();

                Ok(resp)
            }
            Err(err) => {
                let resp = Response::builder()
                    .status(StatusCode::INTERNAL_SERVER_ERROR)
                    .body(Body::from(format!("Error: {}", err)))
                    .unwrap();

                Ok(resp)
            }
        }
    }
}
