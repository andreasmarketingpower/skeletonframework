use crate::*;

pub use cookie::*;

pub struct CookieMiddleware;

impl CookieMiddleware {
    #[allow(clippy::new_without_default)]
    pub fn new() -> CookieMiddleware {
        CookieMiddleware {}
    }
}

impl AroundMiddleware for CookieMiddleware {
    fn wrap_request(&self, req: &mut SkeletonRequest) {
        let mut cookie_jar = CookieJar::new();

        if let Some(cookie_headers) = req.headers().get(hyper::header::COOKIE) {
            let cookie_header_string = cookie_headers.to_str().unwrap().to_string();
            for chunk in cookie_header_string.split(';') {
                if let Ok(cookie) = Cookie::parse(chunk.to_owned()) {
                    cookie_jar.add_original(cookie);
                }
            }
        }

        req.extensions_mut().insert(cookie_jar);
    }

    fn wrap_response(&self, orig: &SkeletonRequest, resp: &mut RawResponse) {
        if let Some(cookie_jar) = orig.extensions().get::<CookieJar>() {
            for c in cookie_jar.delta() {
                let raw_cookie = c.to_string();

                if let Ok(value) = hyper::header::HeaderValue::from_bytes(raw_cookie.as_bytes()) {
                    resp.headers_mut().append("Set-Cookie", value);
                }
            }
        }
    }
}

pub trait Cookies {
    fn cookies(&mut self) -> &mut CookieJar;
}

impl Cookies for SkeletonRequest {
    fn cookies(&mut self) -> &mut CookieJar {
        self
            .extensions_mut()
            .get_mut::<cookies::CookieJar>()
            .unwrap()
    }
}
