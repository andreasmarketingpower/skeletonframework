use crate::*;

pub trait AroundMiddleware {
    fn wrap_request(&self, req: &mut SkeletonRequest);
    fn wrap_response(&self, orig: &SkeletonRequest, resp: &mut RawResponse);

    fn and_then<M: AroundMiddleware>(self, other: M) -> AndThenMiddleware<Self, M>
    where
        Self: Sized,
    {
        AndThenMiddleware {
            m1: self,
            m2: other,
        }
    }
}

pub struct AndThenMiddleware<M1, M2> {
    m1: M1,
    m2: M2,
}

impl<M1, M2> AroundMiddleware for AndThenMiddleware<M1, M2>
where
    M1: AroundMiddleware,
    M2: AroundMiddleware,
{
    fn wrap_request(&self, req: &mut SkeletonRequest) {
        self.m1.wrap_request(req);
        self.m2.wrap_request(req);
    }

    fn wrap_response(&self, orig: &SkeletonRequest, resp: &mut RawResponse) {
        self.m1.wrap_response(orig, resp);
        self.m2.wrap_response(orig, resp);
    }
}

pub struct NoMiddleware;

impl AroundMiddleware for NoMiddleware {
    fn wrap_request(&self, _req: &mut SkeletonRequest) {}
    fn wrap_response(&self, _orig: &SkeletonRequest, _resp: &mut RawResponse) {}
}
