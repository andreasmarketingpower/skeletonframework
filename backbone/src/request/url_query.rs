use std::collections::HashMap;
use url::Url;

use crate::SkeletonRequest;

pub trait UrlQueryExt {
    fn query(&self) -> HashMap<String, String>;
}

impl UrlQueryExt for SkeletonRequest {
    fn query(&self) -> HashMap<String, String> {
        let url = self.uri();

        let url_with_base = format!("http://sth/{}", &url.to_string());
        let parsed = Url::parse(&url_with_base);

        match parsed {
            Ok(form_data) => form_data.query_pairs().into_owned().collect(),
            Err(_) => HashMap::new(),
        }
    }
}
