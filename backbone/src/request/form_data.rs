use std::collections::HashMap;

use async_trait::async_trait;

use crate::SkeletonRequest;
use crate::types::*;
use hyper::body::Body;

#[async_trait]
pub trait FormDataExt {
    async fn form(&mut self) -> HashMap<String, String>;
}

#[async_trait]
impl FormDataExt for SkeletonRequest {
    async fn form(&mut self) -> HashMap<String, String> {
        let body = self.copy_body().await.as_bytes().await.unwrap_or_else(|| vec![]);

        let v = url::form_urlencoded::parse(&body);
        v.into_owned().collect()
    }
}

#[async_trait]
pub trait FormDataBodyExt {
    async fn form(&mut self) -> HashMap<String, String>;
}

#[async_trait]
impl FormDataBodyExt for Body {
    async fn form(&mut self) -> HashMap<String, String> {
        let body = self.as_bytes().await.unwrap_or_else(|| vec![]);

        let v = url::form_urlencoded::parse(&body);
        v.into_owned().collect()
    }
}