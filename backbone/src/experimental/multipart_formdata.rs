#![allow(dead_code)]
use async_trait::async_trait;
use bstr::ByteSlice;

use crate::types::*;
use crate::SkeletonRequest;

#[derive(Debug)]
pub struct MultipartReader<'a> {
    boundary: String,
    body: &'a bstr::BString,
    pos: i32,
}

#[derive(Debug, Default)]
pub struct Field {
    pub name: String,
    pub value: String,

    pub attributes: Vec<Field>,
}

impl Field {
    pub fn new(name: String, value: String) -> Self {
        Field {
            name,
            value,
            attributes: Vec::new(),
        }
    }
}

#[derive(Debug, Default)]
pub struct PartMetadata {
    /// body start_offset
    pub start_offset: usize,
    /// body end_offset
    pub end_offset: usize,

    pub field_name: String,

    pub headers: Vec<Field>,
}

impl PartMetadata {
    pub fn extract_data(&self, body: &bstr::BString) -> Option<Vec<u8>> {
        if self.start_offset >= self.end_offset {
            return None;
        }

        if self.end_offset >= body.len() {
            return None;
        }

        Some(body[self.start_offset..self.end_offset].to_vec())
    }

    pub fn find_header_field_value(&self, name: &str, value: &str) -> Option<&Field> {
        self.headers.iter().find(|hdr| {
            hdr.name.to_lowercase().trim() == name.to_lowercase().trim()
                && hdr.value.to_lowercase().trim() == value.to_lowercase().trim().trim()
        })
    }

    pub fn find_header_attrib(&self, field: &Field, key: &str) -> Option<String> {
        field
            .attributes
            .iter()
            .find(|hdr| hdr.name.to_lowercase().trim() == key.to_lowercase().trim())
            .map(|field| field.value.to_string())
    }

    pub fn get_filename(&self) -> Option<String> {
        self.find_header_field_value("Content-Disposition", "form-data")
            .and_then(|attribs| self.find_header_attrib(&attribs, "filename"))
    }

    pub fn get_mime(&self) -> Option<String> {
        self.headers
            .iter()
            .find(|hdr| hdr.name.to_lowercase().trim() == "content-type")
            .map(|hdr| hdr.value.to_string())
    }
}

#[derive(Debug)]
pub struct PartMetadataList {
    parts: Vec<PartMetadata>,
}

impl PartMetadataList {
    #[allow(clippy::new_without_default)]
    pub fn new() -> Self {
        PartMetadataList { parts: Vec::new() }
    }

    pub fn push(&mut self, part: PartMetadata) {
        self.parts.push(part);
    }

    pub fn contains(&self, name: String) -> Option<bool> {
        for part in &self.parts {
            if part.field_name == name {
                return Some(true);
            }
        }

        None
    }

    pub fn iter(&self) -> std::slice::Iter<PartMetadata> {
        self.parts.iter()
    }

    pub fn find(&self, field_name: &str) -> Option<&PartMetadata> {
        let owned_field_name = field_name.to_string();
        self.parts.iter().find(|part| {
            part.field_name == owned_field_name
                && part
                    .find_header_field_value("Content-Disposition", "form-data")
                    .and_then(|attrib| part.find_header_attrib(&attrib, "filename"))
                    .is_none()
        })
    }

    pub fn find_file(&self, field_name: &str) -> Option<&PartMetadata> {
        let owned_field_name = field_name.to_string();
        self.parts.iter().find(|part| {
            part.field_name == owned_field_name
                && part
                    .find_header_field_value("Content-Disposition", "form-data")
                    .and_then(|attrib| part.find_header_attrib(&attrib, "filename"))
                    .is_some()
        })
    }
}

impl<'a> MultipartReader<'a> {
    pub fn from_reader(body: &'a bstr::BString, boundary: String) -> Self {
        MultipartReader::<'a> {
            body,
            boundary: String::from("--") + &boundary,
            pos: 0,
        }
    }

    pub fn parse(&self) -> PartMetadataList {
        let mut parts_metadata = PartMetadataList::new();

        let boundary_offsets: Vec<usize> = self.body.find_iter(&self.boundary).collect();

        for i in 0..boundary_offsets.len() {
            let j = if i + 1 < boundary_offsets.len() {
                boundary_offsets[i + 1]
            } else {
                self.body.len()
            };

            let start = boundary_offsets[i] + self.boundary.len();
            let end = j;

            let mut part_meta = PartMetadata::default();

            let mut body_index_start = start;
            let mut metadata_started = false;

            for line in self.body[start..end].lines() {
                body_index_start += line.len();

                let line = line.trim();
                let normalized_line = line.to_lowercase();
                let normalized_line = normalized_line.trim();

                if normalized_line.is_empty() && !metadata_started {
                    continue;
                }
                metadata_started = true;

                if normalized_line.is_empty() && metadata_started {
                    break;
                }

                if line.contains_str(b":") {
                    let field_separator = line.find(b":").unwrap();

                    let field = String::from_utf8_lossy(&line[0..field_separator]);
                    let value = String::from_utf8_lossy(if let Some(end_index) = line.find(b";") {
                        &line[field_separator + 1..end_index]
                    } else {
                        &line[field_separator + 1..]
                    });
                    let value = value.trim();

                    let mut new_field = Field::new(field.to_string(), value.to_string());

                    if let Some(attrib_start_index) = line.find(b";") {
                        let attrib_line = String::from_utf8_lossy(&line[attrib_start_index + 1..]);
                        attrib_line.split(';').for_each(|pair| {
                            let key_value: Vec<&str> = pair.split('=').collect();
                            if key_value.len() == 2 {
                                let attrib_name = key_value[0].trim();
                                let attrib_value = key_value[1].trim_matches('"').trim();

                                if attrib_name.to_ascii_lowercase() == "name" {
                                    part_meta.field_name = attrib_value.to_string();
                                }

                                new_field.attributes.push(Field::new(
                                    attrib_name.to_string(),
                                    attrib_value.to_string(),
                                ))
                            }
                        });
                    }

                    part_meta.headers.push(new_field);
                }
            }

            let mut ignore_part = false;
            let maybe_meat_index = self.body[body_index_start..end].find(b"\n");
            if let Some(meat_index) = maybe_meat_index {
                let mut meat_start = body_index_start + meat_index;
                let meat_end = end - 2;

                // Skipping \r\n etc.
                while (self.body[meat_start] == 0x0d || self.body[meat_start] == 0x0a)
                    && meat_start < meat_end
                {
                    meat_start += 1;
                }

                if meat_end >= meat_start {
                    part_meta.start_offset = meat_start;
                    part_meta.end_offset = meat_end;
                } else {
                    ignore_part = true;
                }
            }

            if !ignore_part {
                parts_metadata.push(part_meta);
            }
        }

        parts_metadata
    }
}

pub struct MultipartForm {
    body: bstr::BString,
    parts: PartMetadataList,
}

impl MultipartForm {
    /// field returns a textbox field and tries to parse the value as UTF8,
    /// if that fails then it will try to force it to parse as latin1
    pub fn field(&self, name: &str) -> Option<String> {
        self.parts
            .find(name)
            .and_then(|part| part.extract_data(&self.body))
            .map(|value| match std::str::from_utf8(&value) {
                Ok(_) => String::from_utf8_lossy(&value).to_string(),
                Err(_) => {
                    // not valid UTF8, but at this point we will try latin1
                    let latinsg1: String = value.iter().map(|&c| c as char).collect();
                    latinsg1
                }
            })
    }

    pub fn file_field(&self, name: &str) -> Option<(String, Vec<u8>)> {
        self.parts.find_file(name).and_then(|part| {
            if let Some(body) = part.extract_data(&self.body) {
                Some((part.get_filename().unwrap_or_default(), body))
            } else {
                None
            }
        })
    }

    pub fn field_keys(&self) -> Vec<String> {
        self.parts
            .iter()
            .filter(|part| part.get_filename().is_none())
            .map(|part| part.field_name.clone())
            .collect()
    }

    pub fn file_keys(&self) -> Vec<String> {
        self.parts
            .iter()
            .filter(|part| part.get_filename().is_some())
            .map(|part| part.field_name.clone())
            .collect()
    }
}

#[async_trait]
pub trait SimpleMultipartFormDataExt {
    async fn multipart_form(&mut self) -> Option<MultipartForm>;
}

#[async_trait]
impl SimpleMultipartFormDataExt for SkeletonRequest {
    async fn multipart_form(&mut self) -> Option<MultipartForm> {
        let headers = self.headers();
        let content_type = &headers[hyper::header::CONTENT_TYPE];
        let content_type_str = content_type.to_str().unwrap_or_default().to_owned();

        if content_type_str.contains("boundary=") {
            let boundary_index =
                content_type_str.find("boundary=").unwrap_or(0) + "boundary=".len();
            let boundary = content_type_str[boundary_index..].trim().to_owned();

            let body_bytes = self
                .copy_body()
                .await
                .as_bytes()
                .await
                .unwrap_or_else(|| vec![]);

            let body = bstr::BString::from(body_bytes);
            let reader = MultipartReader::from_reader(&body, boundary.clone());
            let parts = reader.parse();
            drop(reader);

            Some(MultipartForm { body, parts })
        } else {
            None
        }
    }
}

#[cfg(test)]
mod test {
    use log::*;

    #[test]
    fn test_form_boundary_example1() {
        let _ = env_logger::try_init();

        let body: &[u8] = b"----------------------------269862197787164842174022
Content-Disposition: form-data; name=\"name\"

Peter
----------------------------269862197787164842174022
Content-Disposition: form-data; name=\"age\"

21
----------------------------269862197787164842174022
Content-Disposition: form-data; name=\"street\"; filename=\"a-small-imagepixel.png\"
Content-Type: image/png

my Street 44
----------------------------269862197787164842174022";

        let body = bstr::BString::from(body);
        let reader = super::MultipartReader::from_reader(
            &body,
            String::from("--------------------------269862197787164842174022"),
        );

        let parts = reader.parse();
        for part in parts.iter() {
            debug!("Part: {:?}\n", part);
        }
        // assert_eq!(expected, true);
    }
}
