use backbone::experimental::multipart_formdata::*;
use backbone::handlers::file_server::FileServer;
use backbone::middlewares::cookies::*;
use backbone::middlewares::viewengine::tera::*;
use backbone::*;

use serde::{Deserialize, Serialize};

#[tokio::main]
async fn main() {
    let ping_handler = make_controller(Pinger {
        counter: make_shared_data(0),
    });

    // pub fn new(base_query_dir: std::path::PathBuf, base_filesystem_dir: std::path::PathBuf) -> Self {
    let file_server = make_controller(FileServer::new(
        std::path::PathBuf::from("/assets/".to_string()),
        std::path::PathBuf::from("./assets/".to_string()),
    ));

    let mut router = default_router();

    router.add(route!(GET, "/assets/*", file_server.assets));
    router.add(route!(
        GET,
        "/waiter/:seconds",
        ping_handler.waiter,
        response_transformer
    ));

    router.add(route!(GET, "/what1", ping_handler.pong));
    router.add(route!(GET, "/what2", ping_handler.pong));
    router.add(route!(GET, "/template", ping_handler.pong2));
    router.add(route!(POST, "/form", ping_handler.formdata));
    router.add(route!(GET, "/json", ping_handler.json));
    router.add(route!(POST, "/json_request", ping_handler.json_request));

    router.add(route!(GET, "/person", ping_handler.person_json));
    router.add(route!(POST, "/person", ping_handler.person_json_post));
    router.add(route!(POST, "/post", ping_handler.multipart));

    let middlewares = CookieMiddleware::new()
        .and_then(TestMiddleware)
        .and_then(TestMiddleware2)
        .and_then(ViewEngineTeraMiddleware::new("templates/**/*"));

    let web = Web::new(router).with_middlewares(middlewares);

    web.listen("127.0.0.1:1337".parse().unwrap()).await;
}

fn response_transformer(req: ResponseResult) -> ResponseResult {
    match req {
        Ok(resp) => Ok(resp),
        Err(err) => Text(
            format!("-> {} <-", err.to_string()),
            StatusCode::INTERNAL_SERVER_ERROR,
        )
        .into_response(),
    }
}

pub struct TestMiddleware;
impl AroundMiddleware for TestMiddleware {
    fn wrap_request(&self, _req: &mut SkeletonRequest) {
        println!("before");
    }
    fn wrap_response(&self, _orig: &SkeletonRequest, _resp: &mut RawResponse) {
        println!("after")
    }
}

pub struct TestMiddleware2;
impl AroundMiddleware for TestMiddleware2 {
    fn wrap_request(&self, _req: &mut SkeletonRequest) {
        println!("before");
    }
    fn wrap_response(&self, _orig: &SkeletonRequest, _resp: &mut RawResponse) {
        println!("after")
    }
}

pub struct Pinger {
    counter: Shared<i32>,
}

impl Pinger {
    pub async fn pong(&self, req: &mut SkeletonRequest) -> impl SkeletonResponse {
        let value = *self.counter.lock().await;
        req.cookies().add(Cookie::new("foo", format!("{}", value)));

        let url = req.uri().path().to_string();

        {
            let mut value = self.counter.lock().await;
            *value += 1;
        }

        Text(url, StatusCode::OK)
    }

    pub async fn waiter(&self, req: &mut SkeletonRequest) -> impl SkeletonResponse {
        {
            let value = self.counter.lock().await;
            req.cookies().add(Cookie::new("foo", format!("{}", *value)));
        }

        let url = req.uri().path().to_string();
        {
            let mut value = self.counter.lock().await;
            *value += 1;
        }

        let secs = req
            .param("seconds")
            .unwrap_or("1")
            .parse::<u64>()
            .unwrap_or(1);

        if secs > 4 {
            std::fs::File::open("foooooooooooobaaaz")?;
            // let x = Err(static_err("nope"));
            // return x;
        }

        futures_timer::Delay::new(std::time::Duration::from_secs(secs)).await;
        Ok(Text(url, StatusCode::OK))
    }

    pub async fn pong2(&self, req: &mut SkeletonRequest) -> impl SkeletonResponse {
        req.view("home/index.html")
            .set("name", "Whatever")
            .set("age", &33)
            .build()
    }

    pub async fn json(&self, _req: &mut SkeletonRequest) -> impl SkeletonResponse {
        let value = json!({
            "code": 200,
            "counter": *self.counter.lock().await,
            "success": true,
            "payload": {
                "features": [
                    "serde",
                    "json"
                ]
            }
        });

        Json(value, StatusCode::OK)
    }

    pub async fn json_request(&self, req: &mut SkeletonRequest) -> impl SkeletonResponse {
        let value = json!({
            "content": req.into_body().as_string().await.unwrap()
        });

        Json(value, StatusCode::OK)
    }

    pub async fn formdata(&self, req: &mut SkeletonRequest) -> impl SkeletonResponse {
        let form = req.into_body().form().await;
        let mut output = String::new();

        for (key, value) in form.iter() {
            output.push_str(format!("{} = {}\r\n", key, value).as_str());
        }

        let resp = Response::builder()
            .status(StatusCode::OK)
            .body(Body::from(output))
            .unwrap();

        Ok(resp)

        // let mut output: Vec<u8> = Vec::new();
        // while let Some(chunk) = req.body_mut().next().await {
        //     let bytes = chunk.unwrap().into_bytes();
        //     output.extend(&bytes[..]);
        // }
    }

    pub async fn person_json(&self, _req: &mut SkeletonRequest) -> impl SkeletonResponse {
        let person = Person {
            name: "Peter".to_string(),
            age: 22,
            phones: vec!["122-22-33".to_string(), "122-22-44".to_string()],
        };

        Json(person, StatusCode::OK)
    }

    pub async fn person_json_post(&self, req: &mut SkeletonRequest) -> impl SkeletonResponse {
        let person = req.into_body().as_json::<Person>().await.unwrap();

        Json(person, StatusCode::OK)
    }

    pub async fn assets(&self, req: &mut SkeletonRequest) -> impl SkeletonResponse {
        let x = req.uri();
        let x = x.path().trim_start_matches("/assets/");

        Text(String::from(x), StatusCode::OK)
    }

    pub async fn multipart(&self, req: &mut SkeletonRequest) -> impl SkeletonResponse {
        use std::fs::File;
        use std::io::prelude::*;

        let multipart = req.multipart_form().await.unwrap();

        multipart
            .file_field("textfile")
            .and_then(|(filename, value)| {
                let mut file = File::create(filename).unwrap();
                file.write_all(&value).unwrap();
                Some(true)
            });

        multipart
            .file_field("profile_picture")
            .and_then(|(filename, value)| {
                let mut file = File::create(filename).unwrap();
                file.write_all(&value).unwrap();
                Some(true)
            });

        multipart
            .file_field("normal_picture")
            .and_then(|(filename, value)| {
                let mut file = File::create(filename).unwrap();
                file.write_all(&value).unwrap();
                Some(true)
            });

        multipart.field("name").and_then(|value| {
            let mut file = File::create("name.txt").unwrap();
            println!("name value: '{}'", value);
            file.write_all(&value.as_bytes()).unwrap();
            Some(true)
        });

        Text("nothing".to_string(), StatusCode::OK)
    }
}

#[derive(Serialize, Deserialize)]
struct Person {
    name: String,
    age: u8,
    phones: Vec<String>,
}
